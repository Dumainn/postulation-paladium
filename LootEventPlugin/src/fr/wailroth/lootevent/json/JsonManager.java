/*
 * Copyright © 2020 by Enzo Barreiro
 * All rights reserved. This book or any portion thereof
 * may not be reproduced or used in any manner whatsoever
 * without the express written permission of the publisher
 * except for the use of brief quotations in a book review.
 */

package fr.wailroth.lootevent.json;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;


public class JsonManager {

    private JSONObject jsonObject;
    private final JSONParser jsonParser;
    private final File file;

    /**
     *
     * @param file
     */
    public JsonManager(File file) {
        this.jsonObject = new JSONObject();
        this.jsonParser = new JSONParser();
        this.file = file;
//        if(!file.exists()){
//            try {
//                file.createNewFile();
////                file.mkdirs();
//            } catch (IOException e) {
//                System.out.println("unable to create json file " + file.getName() + "|| path: "  + file.getPath() + " || error: " + e.getMessage());
//            }
//        }
    }

    /**
     *
     * @param key
     * @return
     */
    public Object get(Object key){
        return jsonObject.get(key);
    }

    /**
     *
     * @return
     */
    public JsonManager read(){
        try {
            FileReader fileReader = new FileReader(file);
            this.jsonObject = (JSONObject) jsonParser.parse(fileReader);
        } catch (IOException | ParseException e) {
            e.printStackTrace();
        }
        return this;

    }


    /**
     *
     * @param key
     * @param value
     * @return
     */
    public JsonManager set(Object key, Object value){
        jsonObject.put(key,value);
        return this;
    }

    /**
     *
     * @param key
     * @return
     */
    public JsonManager remove(Object key){
        jsonObject.remove(key);
        return this;
    }

    /**
     *
     * @return
     */
    public JsonManager write(){
        try {
            FileWriter fileWriter = new FileWriter(file);
            fileWriter.write(jsonObject.toJSONString());
            fileWriter.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return this;
    }

    public String[] jsonParser(String index){
        String[] reParser = null;

        String[] parser = read().get(index).toString()
                .replace("\"", "")
                .replace("{", "")
                .replace("}", "").split(",");

        for (String var1 : parser) {
            reParser = var1.split(":");
        }

        return reParser;
    }





}
