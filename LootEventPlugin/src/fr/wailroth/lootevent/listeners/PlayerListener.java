package fr.wailroth.lootevent.listeners;

import fr.wailroth.lootevent.Main;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import sun.security.krb5.Config;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/*
 * This code is owned by Alexis Dumain, aliases WailRoth, kaix.
 * This code was created the 12/09/2020
 * Copyright Alexis Dumain - 2020
 */
public class PlayerListener implements Listener {


    private boolean isRunning;

    Main main;

    List<Player> onlinePlayers = new ArrayList<>();

    public PlayerListener(Main main) {
        this.main = main;
    }

    @EventHandler
    public void onJoin(PlayerJoinEvent event) {
        setRunning();
    }


    public void onQuit(PlayerQuitEvent event) {
        onlinePlayers.remove(event.getPlayer());
        setRunning();
    }


    public void setRunning() {
        for (Player p : Bukkit.getServer().getOnlinePlayers()) {
            onlinePlayers.clear();
            onlinePlayers.addAll(Collections.singleton(p));
        }

        if (onlinePlayers.size() >= main.getConfigs().getMinPlayer()) {
            if (isRunning) {
                return;
            }
            isRunning = true;
            main.runTask();
        } else {
            if (isRunning) {
                Bukkit.getScheduler().cancelTask(main.taskID);
                isRunning = false;
            }
        }
    }

}
