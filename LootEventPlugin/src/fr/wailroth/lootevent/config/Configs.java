package fr.wailroth.lootevent.config;

import fr.wailroth.lootevent.Main;
import fr.wailroth.lootevent.utils.Utils;
import lombok.Getter;

/*
 * This code is owned by Alexis Dumain, aliases WailRoth, kaix.
 * This code was created the 24/08/2020
 * Copyright Alexis Dumain - 2020
 */
public class Configs {
    
   @Getter public  String permission;
   @Getter public  String broadcastMessage;
   @Getter public  int minPlayer;

   @Getter public  String startMessageTitle;
   @Getter public  String startMessage;

   @Getter public  String endMessageTitle;
   @Getter public  String endMessage;

    
    
    
    public void init(){
        permission = Main.getInstance().getLootEventConfig().getString("GLOBAL.permission");
        broadcastMessage = Main.getInstance().getLootEventConfig().getString("GLOBAL.broadcast_message");
        minPlayer = Main.getInstance().getLootEventConfig().getInt("GLOBAL.min_player");

        startMessageTitle = Main.getInstance().getLootEventConfig().getString("big_title_start_message.title");
        startMessage = Main.getInstance().getLootEventConfig().getString("big_title_start_message.message");

        endMessageTitle = Main.getInstance().getLootEventConfig().getString("big_title_end_message.title");
        endMessage = Main.getInstance().getLootEventConfig().getString("big_title_end_message.message");

    }
    

}


