package fr.wailroth.lootevent.config;

import fr.wailroth.lootevent.Main;

import java.io.File;
import java.io.IOException;

/*
 * This code is owned by Alexis Dumain, aliases WailRoth, kaix.
 * This code was created the 18/08/2020
 * Copyright Alexis Dumain - 2020
 */
public class CustomConfigFile {
    
    private Main main;

    public CustomConfigFile(Main main) {
        this.main = main;
    }

    public void createConfigFile(String fileName) {
        if (!this.main.getDataFolder().exists()) {
            this.main.getDataFolder().mkdir();
        }

        File file = new File(this.main.getDataFolder(), fileName + ".yml");
        if (!file.exists()) {
            try {
                file.createNewFile();
            } catch (IOException var4) {
                System.out.println("Unable to create config file: " + file.toString());
                var4.printStackTrace();
            }
        }

    }

    public File getFile(String fileName) {
        return new File(this.main.getDataFolder(), fileName + ".yml");
    }

}
