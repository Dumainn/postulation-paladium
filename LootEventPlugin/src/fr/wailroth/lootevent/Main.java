package fr.wailroth.lootevent;

import fr.wailroth.lootevent.commands.LootEventCommand;
import fr.wailroth.lootevent.config.Configs;
import fr.wailroth.lootevent.config.CustomConfigFile;
import fr.wailroth.lootevent.json.IndexedLinkedHashMap;
import fr.wailroth.lootevent.json.JsonManager;
import fr.wailroth.lootevent.listeners.PlayerListener;
import fr.wailroth.lootevent.utils.ItemBuilder;
import fr.wailroth.lootevent.utils.Utils;
import lombok.Getter;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Item;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;
import java.io.IOException;
import java.util.*;
import java.util.concurrent.ThreadLocalRandom;

/*
 * This code is owned by Alexis Dumain, aliases WailRoth, kaix.
 * This code was created the 24/08/2020
 * Copyright Alexis Dumain - 2020
 */
public class Main extends JavaPlugin {

    private FileConfiguration lootEventConfig;
    private FileConfiguration itemFile;

//    private Map<String, Double> basicItemList = new HashMap<>();

    IndexedLinkedHashMap<String, Float> basicItemList = new IndexedLinkedHashMap<>();
    IndexedLinkedHashMap<String, Float> customItemList = new IndexedLinkedHashMap<>();

    CustomConfigFile customConfigFile = new CustomConfigFile(this);

    @Getter
    private List<Integer> itemID = new ArrayList<>();
    private boolean isEventActivated = false;

    public int taskID;

    ItemStack lastItem = null;

    @Getter
    public static Main instance;

    @Getter
    public Configs configs;


    @Override
    public void onEnable() {
        init();
    }

    private void init() {
        instance = this;
        registerFiles();
        configs = new Configs();
        configs.init();
        initBasicJsonFile();
        initCustomJsonFile();
        registerCommands();
        registerListener();
    }


    private void registerCommands() {
        getCommand("lootevent").setExecutor(new LootEventCommand(this));
    }

    private void registerListener() {
        PluginManager pluginManager = Bukkit.getPluginManager();

        pluginManager.registerEvents(new PlayerListener(this), this);

    }

    private void registerFiles() {
        saveResource("loot_event.yml", false);
        lootEventConfig = YamlConfiguration.loadConfiguration(customConfigFile.getFile("loot_event"));
        File file = new File(getDataFolder() + "/items/");
        if (!file.exists()) {
            file.mkdirs();
        }

        if (!lootEventConfig.getBoolean("FILES.created")) {
            createJsonFiles();
        }
    }

    public FileConfiguration getLootEventConfig() {
        return lootEventConfig;
    }

    public void initBasicJsonFile() {
        JsonManager jsonManager = new JsonManager(new File(getDataFolder() + "/items/basic_items.json"));
        basicItemList.put(jsonManager.jsonParser("items")[0], Float.parseFloat(jsonManager.jsonParser("items")[1]));
        jsonManager.set("items", basicItemList).write();

    }

    public void initCustomJsonFile() {
        JsonManager jsonManager = new JsonManager(new File(getDataFolder() + "/items/custom_items.json"));
        customItemList.put(jsonManager.jsonParser("items")[0], Float.parseFloat(jsonManager.jsonParser("items")[1]));
        jsonManager.set("items", customItemList).write();
    }


    public void createJsonFiles() {

        JsonManager jsonBasic = new JsonManager(new File(getDataFolder() + "/items/basic_items.json"));
        JsonManager jsonCustom = new JsonManager(new File(getDataFolder() + "/items/custom_items.json"));

        basicItemList.add("DIAMOND", 10.0f);
        basicItemList.add("IRON_ORE", 10.0f);

        jsonBasic.set("items", basicItemList).write();
        jsonCustom.set("items", basicItemList).write();

        lootEventConfig.set("FILES.created", true);

        try {
            lootEventConfig.save(customConfigFile.getFile("loot_event"));
        } catch (IOException e) {
            e.printStackTrace();
        }

        Bukkit.reload();

    }


    public void runTask() {
        World world = getServer().getWorld(getLootEventConfig().getString("GLOBAL.world"));

        int xMin = getLootEventConfig().getInt("GLOBAL.x.min");
        int xMax = getLootEventConfig().getInt("GLOBAL.x.max");

        int zMin = getLootEventConfig().getInt("GLOBAL.z.min");
        int zMax = getLootEventConfig().getInt("GLOBAL.z.max");

        ProbabilityLaw<String> basicLaw = new ProbabilityLaw<>();
        ProbabilityLaw<String> customLaw = new ProbabilityLaw<>();


        taskID = Bukkit.getServer().getScheduler().scheduleSyncRepeatingTask(this, () -> {


            int x = ThreadLocalRandom.current().nextInt(xMin, xMax);
            int z = ThreadLocalRandom.current().nextInt(zMin, zMax);

            Location location = new Location(world, x, 0, z);
            location.setY(location.getWorld().getHighestBlockYAt(location) + 1);


            //chunk loading
            if (!location.getChunk().isLoaded()) {
                location.getChunk().load();
            }


            //item probability

            basicLaw.clear();
            customLaw.clear();


            for (String key : basicItemList.keySet()) {
                basicLaw.add(key, basicItemList.get(key));
            }

            for (String key : customItemList.keySet()) {
                customLaw.add(key, customItemList.get(key));
            }


            String basicMaterial = basicLaw.draw().orElse("GRASS");

            ItemStack basicItem = new ItemBuilder(Material.getMaterial(basicMaterial), 1).setName("§aLootEvent - §b" + basicMaterial).toItemStack();

            List<Entity> entList = world.getEntities();

            if (lastItem != null) {
                for (Entity current : entList) {
                    if (current instanceof Item) {
                        if (((Item) current).getItemStack() == null) {
                            continue;
                        }
                        if (!((Item) current).getItemStack().hasItemMeta()) {
                            continue;
                        }
                        if (!((Item) current).getItemStack().getItemMeta().hasDisplayName()) {
                            continue;
                        }
                        if (((Item) current).getItemStack().getItemMeta().getDisplayName().equals(lastItem.getItemMeta().getDisplayName())) {
                            current.remove();
                        }
                    }
                }
            }


            if (!itemID.isEmpty()) {
                ItemStack customItem = new ItemBuilder((Material.getMaterial(itemID.get(0))), 1).setName("§aLootEvent - §b" + Material.getMaterial(itemID.get(0)).name()).toItemStack();
                location.getWorld().dropItem(location, customItem);

                broadcastMessage(x, z, location);
                ;

                lastItem = customItem;
                itemID.remove(0);

                return;
            }


            if (isEventActivated) {
                String customMaterial = customLaw.draw().orElse("DIRT");
                ItemStack customItem = new ItemBuilder(Material.getMaterial(customMaterial), 1).setName("§aLootEvent - §b" + customMaterial).toItemStack();
                location.getWorld().dropItem(location, customItem);
                broadcastMessage(x, z, location);

                lastItem = customItem;
            } else {
                location.getWorld().dropItem(location, basicItem);
                System.out.println(location + "|||||" + basicItem.getType().name());

                broadcastMessage(x, z, location);

                lastItem = basicItem;
            }


        }, 0L, 20L * lootEventConfig.getInt("GLOBAL.item_spawn"));
    }

    public void setEventActivated(boolean eventActivated) {
        isEventActivated = eventActivated;
    }

    public boolean isEventActivated() {
        return isEventActivated;
    }


    public void broadcastMessage(int x, int z, Location location) {
        Bukkit.broadcastMessage(Utils.color(configs.getBroadcastMessage())
                .replace("%x%", String.valueOf(x))
                .replace("%z%", String.valueOf(z))
                .replace("%y%", String.valueOf(location.getY()))
        );
    }
}
