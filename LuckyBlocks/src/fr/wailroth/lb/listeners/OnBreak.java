package fr.wailroth.lb.listeners;

import fr.wailroth.lb.Main;
import fr.wailroth.lb.utils.IndexedLinkedHashMap;
import fr.wailroth.lb.utils.ProbabilityLaw;
import org.bukkit.Bukkit;
import org.bukkit.Effect;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;


public class OnBreak implements Listener {

    private final Main main;

    ProbabilityLaw<Integer> luckyLaw = new ProbabilityLaw<>();
    IndexedLinkedHashMap<Integer, Integer> blockLucky = new IndexedLinkedHashMap<>();

    public OnBreak(Main main) {
        this.main = main;
    }

    @EventHandler
    public void onBreakBlock(BlockBreakEvent event) {


        //clear maps
        luckyLaw.clear();
        blockLucky.clear();



        Player player = event.getPlayer();




        String block = null;
        int lucky = 0;


        for (String key : main.getLbConfig().getConfigurationSection("BLOCKS").getKeys(false)) {
            if (event.getBlock().getType().equals(Material.valueOf(main.getLbConfig().getString("BLOCKS." + key + ".material")))) {
                block = main.getLbConfig().getString("BLOCKS." + key + ".material");
                break;
            }

        }


        if (block == null) return;

        if(isInventoryFull(player)){
            player.sendMessage("§cVous ne pouvez pas casser le lucky block. Votre inventaire est plein!");
            event.setCancelled(true);
            return;
        }


        int propLength = main.getLbConfig().getConfigurationSection("BLOCKS." + block + ".PROPERTIES").getKeys(false).size();
        for (int i = 0; propLength > i; i++) {
            blockLucky.put(i, main.getLbConfig().getInt("BLOCKS." + block + ".PROPERTIES." + i + ".chance"));
        }

        for (Integer key : blockLucky.keySet()) {
            luckyLaw.add(key, blockLucky.get(key));
        }

        lucky = luckyLaw.draw().orElse(0);


        int commandsLength = main.getLbConfig().getConfigurationSection("BLOCKS." + block + ".PROPERTIES." +lucky + ".commands").getKeys(false).size();


        for(int i = 0; i < 5; i++){
            event.getBlock().getLocation().getWorld().playEffect(event.getBlock().getLocation(), Effect.MOBSPAWNER_FLAMES, 2003);
        }

        player.playSound(player.getLocation(), Sound.ENDERDRAGON_WINGS, 1, 1);

        for(int i = 0; commandsLength > i; i++){
            Bukkit.dispatchCommand(Bukkit.getServer().getConsoleSender(), main.getLbConfig().getString("BLOCKS." + block + ".PROPERTIES." + lucky + ".commands." + i).replace("player", player.getName()));
        }


        event.setCancelled(true);
        event.getBlock().setType(Material.AIR);



    }


    public boolean isInventoryFull(Player p)
    {
        return p.getInventory().firstEmpty() == -1;
    }
}
